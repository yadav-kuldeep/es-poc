package com.noosyn.poc.es.espoc.search.v1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.noosyn.poc.es.espoc.ESDocument;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import lombok.Getter;

@Getter
@Document(indexName = "product_v1")
@Setting(settingPath = "/settings.json")
public class Product extends ESDocument {
    @Id
    private Long id;
    private Long companyId;
    private String code;
    @Field(type = FieldType.Nested)
    private Map<String, ProductLangAttrs> langAttrs;
    private Long createdOn;
    private Long updatedOn;

    private Product() {
        super(Collections.emptyList());
    }

    private Product(Long id, Long companyId, String code, Map<String, ProductLangAttrs> attrs, List<String> completionTerms, Long createdOn, Long updatedOn) {
        super(completionTerms);
        this.id = id;
        this.companyId = companyId;
        this.code = code;
        this.langAttrs = attrs;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
    }

    public static Product of(Long id, Long companyId, String code, Map<String, ProductLangAttrs> attrs, List<String> completion, Long createdOn, Long updatedOn) {
        return new Product(id, companyId, code, attrs, completion, createdOn, updatedOn);
    }

    public static Product of(Map.Entry<ProductRecord, List<ProductRecord>> e) {
        List<ProductRecord> langAttrs = e.getValue();

        ProductRecord record = e.getKey();

        List<String> completionTerms = new ArrayList<>();
        completionTerms.add(record.getCode());
        
        Map<String, ProductLangAttrs> productLangAttrs = new HashMap<>();
        for(ProductRecord langAttr : langAttrs) {
            ProductLangAttrs attrs = ProductLangAttrs.of(langAttr.getName(), langAttr.getUomName());
            productLangAttrs.put(langAttr.getLangCode(), attrs);

            if(langAttr.getName() != null)
                completionTerms.add(langAttr.getName());
        }

        return Product.of(record.getId(), record.getCompanyId(), record.getCode(), productLangAttrs, completionTerms, record.getCreatedOn(), record.getUpdatedOn());
    }
}
