package com.noosyn.poc.es.espoc;

import lombok.Getter;

@Getter
public class ProductChild {
    private String name;
    private String langCode;
}
