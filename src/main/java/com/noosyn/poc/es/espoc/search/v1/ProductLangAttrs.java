package com.noosyn.poc.es.espoc.search.v1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProductLangAttrs {
    private String name;
    private String uomName;

    public static ProductLangAttrs of(String name, String uomName) {
        return new ProductLangAttrs(name, uomName);
    }
}
