package com.noosyn.poc.es.espoc.nested;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ProductV3Repository extends ElasticsearchRepository<ProductV3, Long> {
    
}
