package com.noosyn.poc.es.espoc.nested;

import java.util.List;

import com.noosyn.poc.es.espoc.ProductChild;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Getter;

@Document(indexName = "product_nested")
@Getter
public class ProductV3 {
    @Id
    private Long id;
    private Long companyId;
    private String code;
    @Field(type = FieldType.Nested)
    private List<ProductChild> children;
    private Long createdOn;
    private Long updatedOn;    
}
