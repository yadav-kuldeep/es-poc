package com.noosyn.poc.es.espoc.search.v1;

import java.util.List;

import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CustomProductESRepositoryImpl implements CustomProductESRepository {
    private final ElasticsearchOperations operations;

    @Override
    public List<Product> findProductSuggestions(String keyword, Long companyId, Integer offset, Integer limit) {
        IndexCoordinates index = IndexCoordinates.of("product_v1");
        
        NativeSearchQuery query = new NativeSearchQueryBuilder()
            .withQuery(QueryBuilders.matchQuery("completionTerms", keyword))
            .withFilter(QueryBuilders.termQuery("companyId", companyId))
            .withPageable(PageRequest.of(offset, limit))
            .build();

        SearchHits<Product> hits = operations.search(query, Product.class, index);
        return  hits.map(hit -> hit.getContent()).toList();
    }
    
}
