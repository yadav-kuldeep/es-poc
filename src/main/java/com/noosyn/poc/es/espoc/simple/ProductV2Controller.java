package com.noosyn.poc.es.espoc.simple;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/v2")
@RequiredArgsConstructor
public class ProductV2Controller {
    private final ProductV2Repository repository;

    @PostMapping("/products")
    public ResponseEntity<Long> createProduct(@RequestBody ProductV2 product) {
        ProductV2 aProduct = repository.save(product);
        return ResponseEntity.ok(aProduct.getId());
    }
    
}
