package com.noosyn.poc.es.espoc.search.v1;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ProductElasticRepository extends ElasticsearchRepository<Product, Long>, CustomProductESRepository{
}
