package com.noosyn.poc.es.espoc.search.v1;

import java.util.List;

public interface CustomProductESRepository {
    List<Product> findProductSuggestions(String keyword, Long companyId, Integer offset, Integer limit);
}
