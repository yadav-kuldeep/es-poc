package com.noosyn.poc.es.espoc.search.v1;

import org.apache.commons.csv.CSVRecord;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(value = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = "id")
public class ProductRecord {
    private Long id;
    private Long companyId;
    private String code;
    private String name;
    private Long uomId;
    private String uomName;
    private String langCode;
    private Long updatedOn;
    private Long createdOn;

    public static ProductRecord of(CSVRecord record) {
        ProductRecord o = new ProductRecord();
        o.setId(Long.valueOf(record.get(0)));
        o.setCompanyId(Long.valueOf(record.get(1)));
        o.setCode(record.get(2));
        o.setName(record.get(3));
        o.setUomId(Long.valueOf(record.get(4)));
        o.setUomName(record.get(5));
        o.setLangCode(record.get(6));
        o.setUpdatedOn(Long.valueOf(record.get(7)));
        o.setCreatedOn(Long.valueOf(record.get(8)));

        return o;
    }
}
