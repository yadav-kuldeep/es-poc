package com.noosyn.poc.es.espoc.search.v1;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/productsSearch")
public class ProductSearchController {
    private final ProductElasticRepository repository;

    @GetMapping
    public ResponseEntity<List<Product>> suggestProducts(@RequestParam String keyword, @RequestParam Long companyId, @RequestParam Integer offset, @RequestParam Integer limit) {
        return ResponseEntity.ok(repository.findProductSuggestions(keyword, companyId, offset, limit));
    }
}
